#!/bin/ash
if ls /etc/ssh/keys/ssh_host_* &> /dev/null 2>&1; then
    echo Host Key Found
else
    rm -rf /etc/ssh/keys/*
    ssh-keygen -t rsa -b 4096 -f  /etc/ssh/keys/ssh_host_key
    cp /etc/ssh/keys/ssh_host_key /etc/ssh/keys/ssh_host_rsa_key
    ssh-keygen -t dsa -b 1024 -f /etc/ssh/keys/ssh_host_dsa_key
    ssh-keygen -t ecdsa -f /etc/ssh/keys/ssh_host_ecdsa_key
    ssh-keygen -t ed25519 -f /etc/ssh/keys/ssh_host_ed25519_key
fi

# Own known Keys
chown -R $SSH_USER:$SSH_USER /home/$SSH_USER
chown -R $SSH_USER:$SSH_USER /home/$SSH_USER/.ssh
/usr/sbin/sshd -4 -o AllowTcpForwarding=yes -o GatewayPorts=yes -D -e
