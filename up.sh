#!/bin/bash
echo "Welcome :) This is the doccam docker setup script."
echo "Follow the prompts carefully and store a transcript at a SAVE location."
echo "If you want to reset the config options, please run this command with"
echo "the RESET Argument."
echo "======================================================================"
echo

cd "$(dirname "$0")"

source lib/mo

# Load the Config
if [ $# -eq 0 ]; then
    source .env
else
    rm -r volumes/letsencrypt/
fi

if [ -z ${DOCCAM_HOST+x} ]; then
    printf 'Enter the hostname for the web interface and press [ENTER]: '; read DOCCAM_HOST
fi
if [ -z ${LETSENCRYPT_EMAIL+x} ]; then
    printf 'Enter the email address that shall be used for the letsencrypt certificate generator and press [ENTER]: '; read LETSENCRYPT_EMAIL
fi
if [ -z ${USERNAME+x} ]; then
    printf 'Enter the username for the web interface and press [ENTER]: '; read USERNAME
fi
if [ -z ${PASSWORD+x} ]; then
    printf 'Enter the password for the web interface and press [ENTER]: '; read PASSWORD
fi
if [ -z ${SSH_USER+x} ]; then
    printf 'Enter the username for the ssh server and press [ENTER]: '; read SSH_USER
fi
if [ -z ${SSH_PASSWORD+x} ]; then
    printf 'Enter the password for the ssh server and press [ENTER]: '; read SSH_PASSWORD
fi
if [ -z ${SSH_PORT+x} ]; then
    printf 'Enter the port number for the ssh server and press [ENTER]: '; read SSH_PORT
fi
if [ -z ${CLOUDFLARE_EMAIL+x} ]; then
    printf 'Enter Cloudflare EMAIL and press [ENTER]: '; read CLOUDFLARE_EMAIL
fi
if [ -z ${CLOUDFLARE_API_KEY+x} ]; then
    printf 'Enter Cloudflare API KEY and press [ENTER]: '; read CLOUDFLARE_API_KEY
fi

cat >.env <<EOL
DOCCAM_HOST=$DOCCAM_HOST
LETSENCRYPT_EMAIL=$LETSENCRYPT_EMAIL
USERNAME=$USERNAME
PASSWORD=$PASSWORD
SSH_USER=$SSH_USER
SSH_PASSWORD=$SSH_PASSWORD
SSH_PORT=$SSH_PORT
CLOUDFLARE_EMAIL=$CLOUDFLARE_EMAIL
CLOUDFLARE_API_KEY=$CLOUDFLARE_API_KEY
EOL

# regenerate nginx config
mkdir -p volumes/nginx/
cat templates/nginx/app.conf.template | mo > volumes/nginx/app.conf
docker run --rm -ti xmartlabs/htpasswd $USERNAME $PASSWORD > volumes/nginx/htpasswd

if ! [[ -d volumes/letsencrypt/ ]]; then
    echo 'No certificates detected. Initializing...'
    bootstrap/bootstrap.sh

    # copy the generated certs
    cp -r bootstrap/out/letsencrypt/ volumes/
fi

docker-compose pull
docker-compose build ssh
docker-compose up -d
