#!/bin/bash
cd "$(dirname "$0")"


source ../.env

Cleanup() {
    echo "Exiting..."
    popd 2> /dev/null || true
}

trap Cleanup EXIT


# create tmp dir
mkdir -p out/

# create cloudflare credentials
mkdir -p out/letsencrypt/
cat > out/letsencrypt/cloudflare <<EOF
dns_cloudflare_email = $CLOUDFLARE_EMAIL
dns_cloudflare_api_key = $CLOUDFLARE_API_KEY
EOF
chmod 600 out/letsencrypt/cloudflare

# SSL config
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "out/letsencrypt/options-ssl-nginx.conf"
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "out/letsencrypt/ssl-dhparams.pem"

# get the certificates
docker run -it --rm \
       -v "$PWD"/out/letsencrypt:/etc/letsencrypt \
       -v "$PWD"/letsencrypt-site:/data/letsencrypt \
       certbot/dns-cloudflare \
       certonly --dns-cloudflare \
       --email $LETSENCRYPT_EMAIL --agree-tos \
       --dns-cloudflare-credentials /etc/letsencrypt/cloudflare \
       -d $DOCCAM_HOST -d *.$DOCCAM_HOST
