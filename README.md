# Doccam Server Docker Compose
This repo contains a docker based system to deploy [doccam-server](https://gitlab.com/vale9811/doccam-server) with near-to-zero configuration effort.

The docker ensemble consists of a `ssh` server for tunneling, the
`doccam-server` software and an ngninx proxy and certbot for
certificate renewal.

## Requirements
- Docker
- Docker Compose
- DNS records pointing to the machine that is running this: `domain.tld`, and `*.domain.tld` on cloudflare.
- A cloudflare API key

## How to deploy
1. Pull this Repo
2. Run the script `./up.sh` as privileged user and follow the propmpts.

This will create the https certificates and start the services.  You
can stop the services at any time by running `down.sh`. The
configuration is retained in the `.env` file. It is recommendet to set
the correct permissions on the whole setup to ensure security.
