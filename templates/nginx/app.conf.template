server {
    listen 80;
    server_name {{ DOCCAM_HOST }};

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl;
    server_name {{ DOCCAM_HOST }};
    ssl_certificate certs/live/{{ DOCCAM_HOST }}/fullchain.pem;
    ssl_certificate_key certs/live/{{ DOCCAM_HOST }}/privkey.pem;

    # SSL config from letsencrypt
    include certs/options-ssl-nginx.conf;
    ssl_dhparam certs/ssl-dhparams.pem;

    # authentication
    auth_basic "Cam Panel";
    auth_basic_user_file /etc/nginx/conf.d/htpasswd;

    location / {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;

        proxy_pass http://doccam-server:8080;
    }
}

server {
    listen 443 ssl;
    server_name   ~^(www\.)?cam(?<subdomain>[0-9]+?)\..*$;
    ssl_certificate certs/live/{{ DOCCAM_HOST }}/fullchain.pem;
    ssl_certificate_key certs/live/{{ DOCCAM_HOST }}/privkey.pem;

    # SSL config from letsencrypt
    include certs/options-ssl-nginx.conf;
    ssl_dhparam certs/ssl-dhparams.pem;

    location / {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;

        resolver 127.0.0.11;  # use the docker reslover
        proxy_pass http://ssh:$subdomain$request_uri;
    }
}
